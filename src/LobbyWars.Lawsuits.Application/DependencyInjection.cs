﻿using LobbyWars.Lawsuits.Application.Models;
using Microsoft.Extensions.DependencyInjection;

namespace LobbyWars.Lawsuits.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddSingleton(typeof(Result<>));
        services.AddScoped<ISignatoryService, SignatoryService>();
        services.AddScoped<IContractService, ContractService>();
        services.AddMediatR(typeof(DependencyInjection).Assembly);
        return services;
    }
}
