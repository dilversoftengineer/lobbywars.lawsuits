﻿namespace LobbyWars.Lawsuits.Application.Models;

public class ContractSignatureModel
{
    public ContractSignatureModel(Signatory signatory, LawsuitParty lawsuitParty)
    {
        Signatory = signatory ?? throw new ArgumentNullException(nameof(signatory));
        LawsuitParty = lawsuitParty ?? throw new ArgumentNullException(nameof(lawsuitParty));
    }
    public Signatory Signatory { get; private set; }
    public LawsuitParty LawsuitParty { get; private set; }
}
