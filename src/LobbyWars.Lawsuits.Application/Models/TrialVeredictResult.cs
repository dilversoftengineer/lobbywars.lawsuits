﻿namespace LobbyWars.Lawsuits.Application.Models
{
    public class TrialVeredictResult
    {
        public TrialVeredictResult() { }
        public TrialVeredictResult(string lawsuitPartyWinner, short points, string trialNumber)
        {
            LawsuitPartyWinner = lawsuitPartyWinner;
            Points = points;
            TrialNumber = trialNumber ?? throw new ArgumentNullException(nameof(trialNumber));
        }

        public string LawsuitPartyWinner { get; set; }
        public short Points { get; set; }
        public string TrialNumber { get; set; }
        public bool ThereIsWinner => !string.IsNullOrEmpty(LawsuitPartyWinner);
    }
}
