﻿namespace LobbyWars.Lawsuits.Application.SeedWork;

public enum ResultCodeEnum
{
    Ok = 200,
    Error = 500,
    BadRequest= 400
}
