﻿namespace LobbyWars.Lawsuits.Application.SeedWork
{
    /// <summary>
    /// mapper
    /// </summary>
    public interface IApplicationMapper
    {
        /// <summary>
        /// map entity to class
        /// </summary>
        /// <typeparam name="I"><see cref="{T}"/></typeparam>
        /// <param name="source">object to map</param>
        Task<I> Map<I>(object source) where I : class;
    }
}
