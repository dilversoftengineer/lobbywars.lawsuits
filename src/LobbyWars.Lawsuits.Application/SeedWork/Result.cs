﻿namespace LobbyWars.Lawsuits.Application.Models;

public sealed class Result<T>
{
    readonly ILogger _logger;

    public Result(ILogger logger) => _logger = logger;
    public ResultCodeEnum Code { get; set; } = ResultCodeEnum.Ok;
    public IEnumerable<string> Messages { get; set; } = Enumerable.Empty<string>();
    public T Data { get; set; }
    public IEnumerable<string> Errors { get; set; } = Enumerable.Empty<string>();

    public void AppendMessage(string message) => Messages = Messages.Append(message);
    public void AppendError(LawsuitsDomainException exception, string message = "")
    {
        string errorMessage = string.IsNullOrEmpty(message) ? exception.Message : message;
        Errors = Errors.Append(errorMessage);
        Code = ResultCodeEnum.Error;
        _logger.LogError(exception, errorMessage);
    }
    public void AppendError(string message)
    {
        Errors = Errors.Append(message);
        Code = ResultCodeEnum.Error;
        _logger.LogError($"Error: {message}");
    }
}
