﻿using LobbyWars.Lawsuits.Application.Models;
using LobbyWars.Lawsuits.Application.UseCases.Signatory;
using LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;
using LobbyWars.Lawsuits.Domain.Aggreagates.SignatoryAggregate;
using LobbyWars.Lawsuits.Domain.SeedWork;

namespace LobbyWars.Lawsuits.Application.Services;
public class ContractService : IContractService
{
    private readonly ILogger<ContractService> _logger;
    private readonly ISignatoryService _signatoryService;
    public ContractService(ILogger<ContractService> logger, ISignatoryService signatoryService)
    {
        _logger = logger;
        _signatoryService = signatoryService;
    }
    public async Task<Result<TrialVeredictResult>> DetermineLawsuitPartyWinner(string trialNumber, IReadOnlyCollection<ContractSignatureModel> contractSignatures)
    {
        Result<TrialVeredictResult> result = new(_logger);
        try
        {
            LawsuitParty partyInvolvedWinner = new(0, string.Empty);
            var signatures = contractSignatures.GroupBy(s => s.LawsuitParty);

            short pointsOfWinnerParty = 0;
            foreach (IGrouping<LawsuitParty, ContractSignatureModel> signaturesGroup in signatures)
            {
                var groupSignatories = signaturesGroup.Select(s => s.Signatory);
                Result<short> sumPointsResult = await _signatoryService.SumPoints(groupSignatories);
                if (sumPointsResult.Code == ResultCodeEnum.Ok)
                {
                    short points = sumPointsResult.Data;
                    if (points > pointsOfWinnerParty)
                    {
                        partyInvolvedWinner = signaturesGroup.Key;
                        pointsOfWinnerParty = points;
                    }
                    else if (points == pointsOfWinnerParty)
                        partyInvolvedWinner = new(0, string.Empty);
                }
            }
            result.Data = new TrialVeredictResult(partyInvolvedWinner.Name, pointsOfWinnerParty, trialNumber);
        }
        catch (LawsuitsDomainException e) { result.AppendError(e); }
        return result;
    }
    public async Task<Result<IEnumerable<ContractSignatureModel>>> GetContractSignaturesByLawsuitPartiesAsync(char[] plaintiffSignatures, char[] defendantSignatures, IReadOnlyCollection<Signatory> signatories)
    {
        Result<IEnumerable<ContractSignatureModel>> result = new(_logger);
        try
        {
            plaintiffSignatures = plaintiffSignatures.Where(l => l != '#').ToArray();
            defendantSignatures = defendantSignatures.Where(l => l != '#').ToArray();

            var plaintiffSignatoriesResult = await GetContractSignaturesByKeysAsync(plaintiffSignatures, LawsuitParty.Plaintiff, signatories);
            var defendantSignatoriesResult = await GetContractSignaturesByKeysAsync(defendantSignatures, LawsuitParty.Defendant, signatories);

            if (plaintiffSignatoriesResult.Code == ResultCodeEnum.Ok && defendantSignatoriesResult.Code == ResultCodeEnum.Ok)
                result.Data = plaintiffSignatoriesResult.Data.Concat(defendantSignatoriesResult.Data);
            else
                result.Code = ResultCodeEnum.Error;
        }
        catch (LawsuitsDomainException e) { result.AppendError(e); }
        return result;
    }
    public async Task<Result<IEnumerable<ContractSignatureModel>>> GetContractSignaturesByKeysAsync(char[] signaturesKeys, LawsuitParty lawsuitParty, IReadOnlyCollection<Signatory> signatories)
    {
        Result<IEnumerable<ContractSignatureModel>> result = new(_logger);
        try
        {
            var lawsuitPartySignatoriesResult = await _signatoryService.GetSignatoriesByKeys(signaturesKeys, signatories);
            if (lawsuitPartySignatoriesResult.Code == ResultCodeEnum.Ok)
                if (lawsuitPartySignatoriesResult.Data.Count > 0)
                    result.Data = lawsuitPartySignatoriesResult.Data.Select(sinatory => new ContractSignatureModel(sinatory, lawsuitParty));
                else result.Code = ResultCodeEnum.Error;
        }
        catch (LawsuitsDomainException e) { result.AppendError(e); }
        return result;
    }


}