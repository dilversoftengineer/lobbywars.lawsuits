﻿namespace LobbyWars.Lawsuits.Application.Services;

public interface IContractService
{
    /// <summary>
    /// Determine which lawsuit party wins the test
    /// </summary>
    /// <param name="trialNumber">trial number</param>
    /// <param name="contractSignatures">contract signatures</param>
    /// <returns>Trail veredict</returns>
    Task<Result<TrialVeredictResult>> DetermineLawsuitPartyWinner(string trialNumber, IReadOnlyCollection<ContractSignatureModel> contractSignatures);
    Task<Result<IEnumerable<ContractSignatureModel>>> GetContractSignaturesByLawsuitPartiesAsync(char[] plaintiffSignatures, char[] defendantSignatures, IReadOnlyCollection<Signatory> signatories);
}
