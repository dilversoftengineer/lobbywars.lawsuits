﻿namespace LobbyWars.Lawsuits.Application.Services;

public interface ISignatoryService : IDisposable
{
    /// <summary>
    /// Sum points of a signatory list
    /// </summary>
    /// <param name="signers">signatories</param>
    /// <remarks> <see cref="Signatory"/></remarks>
    /// <returns>Total points</returns>
    Task<Result<short>> SumPoints(IEnumerable<Signatory> signers);
    /// <summary>
    ///    Get signatories based on key array 
    /// </summary>
    /// <param name="signatureKeys">keys array</param>
    /// <param name="signatories">base signatories</param>
    /// <remarks> <see cref="Signatory"/></remarks>
    Task<Result<IReadOnlyCollection<Signatory>>> GetSignatoriesByKeys(char[] signatureKeys, IReadOnlyCollection<Signatory> signatories);
    Task<Result<IDictionary<LawsuitParty, Signatory>>> GetSignatoriesRequiredToWinByLawsuitParty(IEnumerable<ContractSignatureModel> contractSignatures, IEnumerable<Signatory> signatories);
}
