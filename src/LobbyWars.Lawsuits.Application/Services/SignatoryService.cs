﻿namespace LobbyWars.Lawsuits.Application.Services;
public sealed class SignatoryService : ISignatoryService
{
    private readonly ILogger<SignatoryService> _logger;
    public SignatoryService(ILogger<SignatoryService> logger) => _logger = logger;
    public Task<Result<short>> SumPoints(IEnumerable<Signatory> signers)
    {
        Result<short> result = new(_logger);
        short points = 0;
        try
        {
            bool signersContainKing = signers.Any(s => s.Key == SignatoryRoleKey.K);
            foreach (Signatory signer in signers)
            {
                if (signer.Key == SignatoryRoleKey.V && signersContainKing) continue;
                points += signer.Points;
            }
            result.Data = points;
        }
        catch (LawsuitsDomainException e) { result.AppendError(e); }
        return Task.FromResult(result);
    }
    public async Task<Result<IReadOnlyCollection<Signatory>>> GetSignatoriesByKeys(char[] signatureKeys, IReadOnlyCollection<Signatory> signatories)
    {
        Result<IReadOnlyCollection<Signatory>> result = new(_logger);
        try
        {
            List<Signatory> signers = new();
            foreach (char signatureKey in signatureKeys)
            {
                signers.Add(signatories.First(s => s.Key.Name.First() == signatureKey));
            }
            result.Data = signers;
        }
        catch (LawsuitsDomainException e) { result.AppendError(e); }
        return await Task.FromResult(result);
    }
    public async Task<Result<IDictionary<LawsuitParty, Signatory>>> GetSignatoriesRequiredToWinByLawsuitParty(IEnumerable<ContractSignatureModel> contractSignatures, IEnumerable<Signatory> signatories)
    {
        Result<IDictionary<LawsuitParty, Signatory>> result = new(_logger);
        result.Data = new Dictionary<LawsuitParty, Signatory>();
        try
        {
            var singaturesByLawsuitParty = contractSignatures.GroupBy(l => l.LawsuitParty);
            foreach (var group in singaturesByLawsuitParty)
            {
                LawsuitParty lawsuitParty = group.Key;
                var oppositionSignatures = contractSignatures.Where(l => l.LawsuitParty != lawsuitParty).Select(l => l.Signatory);
                var oppositionPointsResult = await SumPoints(oppositionSignatures);

                Result<Signatory> signatoryRequiredToWin = await DetermineSignatoryRequiredToWin(group.Select(s => s), oppositionPointsResult.Data, signatories);
                if (signatoryRequiredToWin.Code == ResultCodeEnum.Ok)
                    result.Data.Add(lawsuitParty, signatoryRequiredToWin.Data);
            }
        }
        catch (LawsuitsDomainException e) { result.AppendError(e); }
        return result;
    }
    async Task<Result<Signatory>> DetermineSignatoryRequiredToWin(IEnumerable<ContractSignatureModel> lawsuitParty, short oppositionPoints, IEnumerable<Signatory> signatories)
    {
        Result<Signatory> result = new(_logger);
        try
        {
            var lawsuitPartySignatories = lawsuitParty.Select(l => l.Signatory);
            var lawsuitPartyPointsResult = await SumPoints(lawsuitPartySignatories);
            if (lawsuitPartyPointsResult.Code == ResultCodeEnum.Ok)
            {
                if (lawsuitPartyPointsResult.Data > oppositionPoints) return result;

                foreach (Signatory signatory in signatories.OrderBy(x => x.Points))
                {
                    var localSignatories = lawsuitPartySignatories.Append(signatory);
                    var pointsResult = await SumPoints(localSignatories);
                    if (pointsResult.Data > oppositionPoints)
                    {
                        result.Data = signatory;
                        break;
                    }
                }
            }
        }
        catch (LawsuitsDomainException e) { result.AppendError(e); }
        return result;
    }

    public void Dispose()
    {
        //Dispose();
        //GC.SuppressFinalize(this);
    }

}