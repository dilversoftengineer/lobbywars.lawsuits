﻿namespace LobbyWars.Lawsuits.Application.UseCases.Contract.Commands;

public record DetermineWinnerCommand(string TrialNumber, string PlaintiffSignatures, string DefendantSignatures) : IRequest<Result<DetermineWinnerCommandResult>>;
public record DetermineWinnerCommandResult(string LawsuitPartyWinner, short Points, string TrialNumber, bool ThereIsWinner);
public class DetermineWinnerCommandHandler : IRequestHandler<DetermineWinnerCommand, Result<DetermineWinnerCommandResult>>, IDisposable
{
    private volatile bool _disposed;
    private readonly ILogger<DetermineWinnerCommandHandler> _logger;
    private readonly IContractRepositoryWriter _contractRepositoryWriter;
    private readonly ITrialVereditRepositoryWriter _trialVereditRepositoryWriter;
    private readonly ISignatoryRepositoryReader _signatoryRepositoryReader;
    private readonly ISignatoryService _signatoryService;
    private readonly IContractService _contractService;

    public DetermineWinnerCommandHandler(ILogger<DetermineWinnerCommandHandler> logger,
        IContractRepositoryWriter contractRepositoryWriter,
        ISignatoryRepositoryReader signatoryRepositoryReader,
        ISignatoryService signatoryService,
        IContractService contractService,
        ITrialVereditRepositoryWriter trialVereditRepositoryWriter)
    {
        _logger = logger;
        _contractRepositoryWriter = contractRepositoryWriter;
        _signatoryRepositoryReader = signatoryRepositoryReader;
        _signatoryService = signatoryService;
        _contractService = contractService;
        _trialVereditRepositoryWriter = trialVereditRepositoryWriter;
    }
    public async Task<Result<DetermineWinnerCommandResult>> Handle(DetermineWinnerCommand request, CancellationToken cancellationToken)
    {
        Result<DetermineWinnerCommandResult> result = new(_logger);
        try
        {
            Domain.Aggreagates.ContractAggregate.Contract contract = new(request.TrialNumber);

            var signatories = await _signatoryRepositoryReader.All();

            List<ContractSignatureModel> contractSignatures = new();
            await GetLawsuitPartySignatoriesByKeysAsync(request.PlaintiffSignatures.ToArray(), LawsuitParty.Plaintiff);
            await GetLawsuitPartySignatoriesByKeysAsync(request.DefendantSignatures.ToArray(), LawsuitParty.Defendant);

            Result<TrialVeredictResult> trialVeredictResult = await _contractService.DetermineLawsuitPartyWinner(request.TrialNumber, contractSignatures);
            TrialVeredictResult trialVeredict = trialVeredictResult.Data;
            if (trialVeredictResult.Code == ResultCodeEnum.Ok && trialVeredict.ThereIsWinner)
            {
                contract.AddSignatures(contractSignatures.Select(s => new ContractSignature(contract.Id, s.Signatory.Id, s.LawsuitParty)));
                
                await _contractRepositoryWriter.AddAsync(contract);

                if (await _contractRepositoryWriter.UnitOfWork.SaveEntityChangesAsync(cancellationToken))
                {
                    await _trialVereditRepositoryWriter.AddAsync(new TrialVeredict(trialVeredict.LawsuitPartyWinner, trialVeredict.Points, contract.TrialNumber));
                    await _trialVereditRepositoryWriter.UnitOfWork.SaveEntityChangesAsync(cancellationToken);
                }
                else
                {
                    result.AppendError("Ocurrió un error al guardar el contrato...");
                    return result;
                }
            }
            result.Data = new DetermineWinnerCommandResult(trialVeredict.LawsuitPartyWinner, trialVeredict.Points, trialVeredict.TrialNumber, trialVeredict.ThereIsWinner);


            async Task GetLawsuitPartySignatoriesByKeysAsync(char[] signaturesKeys, LawsuitParty lawsuitParty)
            {
                var lawsuitPartySignatoriesResult = await _signatoryService.GetSignatoriesByKeys(signaturesKeys, signatories);
                if (lawsuitPartySignatoriesResult.Code == ResultCodeEnum.Ok)
                {
                    if (lawsuitPartySignatoriesResult.Data.Count > 0)
                        contractSignatures.AddRange(lawsuitPartySignatoriesResult.Data.Select(sinatory => new ContractSignatureModel(sinatory, lawsuitParty)));
                }
            }
        }
        catch (LawsuitsDomainException e)
        {
            result.AppendError(e);
        }
        return result;
    }
    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _contractRepositoryWriter.Dispose();
            }
            _disposed = true;
        }
    }
    ~DetermineWinnerCommandHandler() { Dispose(disposing: false); }
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
