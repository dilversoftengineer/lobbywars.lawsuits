﻿using LobbyWars.Lawsuits.Application.UseCases.Contract.Commands;

namespace LobbyWars.Lawsuits.Application.UseCases.MinimumSignatureToWin
{
    public record MinimumSignatureToWinCommand(string TrialNumber, string PlaintiffSignatures, string DefendantSignatures) : IRequest<MinimumSignatureToWinCommandResult>;
    public record MinimumSignatureToWinCommandResult(TrialVeredictResult TrialVeredict);

    public class MinimumSignatureToWinCommandHandler : IRequestHandler<DetermineWinnerCommand, DetermineWinnerCommandResult>
    {
        public Task<DetermineWinnerCommandResult> Handle(DetermineWinnerCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
