﻿namespace LobbyWars.Lawsuits.Application.UseCases.Signatory.Queries;

public record GetAllSignatoriesQuery() : IRequest<Result<IReadOnlyCollection<GetAllSignatoriesQueryResult>>>;
public record GetAllSignatoriesQueryResult(Guid Id, string Key, string Name, short Points);

public class GetAllSignatoriesQueryHandler : IRequestHandler<GetAllSignatoriesQuery, Result<IReadOnlyCollection<GetAllSignatoriesQueryResult>>>
{
    private readonly ILogger<GetAllSignatoriesQueryHandler> _logger;
    private readonly IApplicationMapper _mapper;
    private readonly ISignatoryRepositoryReader _signatoryQueryRepository;

    public GetAllSignatoriesQueryHandler(ILogger<GetAllSignatoriesQueryHandler> logger, ISignatoryRepositoryReader signatoryQueryRepository, IApplicationMapper mapper)
    {
        _logger = logger;
        _signatoryQueryRepository = signatoryQueryRepository;
        _mapper = mapper;
    }

    public async Task<Result<IReadOnlyCollection<GetAllSignatoriesQueryResult>>> Handle(GetAllSignatoriesQuery request, CancellationToken cancellationToken)
    {
        Result<IReadOnlyCollection<GetAllSignatoriesQueryResult>> result = new(_logger);
        try
        {
            result.Data = await _mapper.Map<IReadOnlyCollection<GetAllSignatoriesQueryResult>>(await _signatoryQueryRepository.All());
        }
        catch (LawsuitsDomainException e)
        {
            result.AppendError(e);
        }
        return result;
    }

    public void Dispose()
    {
        Dispose();
        GC.SuppressFinalize(this);
    }
}
