﻿using LobbyWars.Lawsuits.Domain.SeedWork;
namespace LobbyWars.Lawsuits.Application.UseCases.Signatory;
/// <summary>
/// Manual validations
/// </summary>
public static class SignatureValidations
{
    /// <summary>
    /// Verify that the signatures of the parties to the lawsuit are correct
    /// </summary>
    /// <param name="plaintiffSignatures">Plaintiff signatures keys</param>
    /// <param name="defendantSignatures">Defendant signatures keys</param>
    /// <returns>Errors</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static Task<string[]> VerifySignaturesAreCorrect(char[] plaintiffSignatures, char[] defendantSignatures)
    {
        if (plaintiffSignatures is null)
            throw new ArgumentNullException(nameof(plaintiffSignatures));

        if (defendantSignatures is null)
            throw new ArgumentNullException(nameof(defendantSignatures));

        List<string> errors =new();

        bool valuelessExceded = plaintiffSignatures.Where(l => l == SignatoryRoleKey.Valueless.Name.First()).Count() > 1 ||
                                       defendantSignatures.Where(l => l == SignatoryRoleKey.Valueless.Name.First()).Count() > 1;
        if (valuelessExceded)
            errors.Add("Only one signature per part can be empty to be valid");

        var signaturesToValidate = plaintiffSignatures.Concat(defendantSignatures).Select(char.ToUpper);
        var signaturesAllowed = Enumeration.GetAll<SignatoryRoleKey>().Select(k => k.Name.ToUpper().First());
        bool hasInvalidSignatures = false;
        foreach (char key in signaturesToValidate)
        {
            if (!signaturesAllowed.Contains(key))
            {
                hasInvalidSignatures = true;
                break;
            }
        }

        if (hasInvalidSignatures)
            errors.Add($"Request contains invalid signatures. Valid signing keys ({new string(signaturesAllowed.ToArray())})");

        return Task.FromResult(errors.ToArray());
    }
}
