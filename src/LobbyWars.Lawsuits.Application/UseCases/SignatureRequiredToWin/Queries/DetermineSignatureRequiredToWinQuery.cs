﻿using LobbyWars.Lawsuits.Application.UseCases.Signatory;

namespace LobbyWars.Lawsuits.Application.UseCases.SignatureRequiredToWin.Commands;

public record DetermineSignatureRequiredToWinQuery(string PlaintiffSignatures, string DefendantSignatures) : IRequest<Result<DetermineSignatureRequiredToWinQueryResult>>;
public record DetermineSignatureRequiredToWinQueryResult(string PlaintiffSignaturedRequiredToWin, string DefendantSignatureRequiredToWin);
public class DetermineSignatureRequiredToWinQueryHandler : IRequestHandler<DetermineSignatureRequiredToWinQuery, Result<DetermineSignatureRequiredToWinQueryResult>>, IDisposable
{
    private volatile bool _disposed;
    private readonly ILogger<DetermineSignatureRequiredToWinQueryHandler> _logger;
    private readonly ISignatoryService _signatoryService;
    private readonly IContractService _contractService;
    private readonly ISignatoryRepositoryReader _signatoryRepositoryReader;

    public DetermineSignatureRequiredToWinQueryHandler(ILogger<DetermineSignatureRequiredToWinQueryHandler> logger,
        ISignatoryService signatoryService,
        IContractService contractService,
        ISignatoryRepositoryReader signatoryRepositoryReader)
    {
        _logger = logger;
        _signatoryService = signatoryService;
        _contractService = contractService;
        _signatoryRepositoryReader = signatoryRepositoryReader;
    }
    public async Task<Result<DetermineSignatureRequiredToWinQueryResult>> Handle(DetermineSignatureRequiredToWinQuery request, CancellationToken cancellationToken)
    {
        Result<DetermineSignatureRequiredToWinQueryResult> result = new(_logger);
        try
        {
            string[] signaturesValidationErrors = await SignatureValidations.VerifySignaturesAreCorrect(request.PlaintiffSignatures.ToArray(), request.DefendantSignatures.ToArray());
            if (signaturesValidationErrors.Length > 0)
            {
                result.Code = ResultCodeEnum.BadRequest;
                result.Errors = result.Errors.Concat(signaturesValidationErrors.ToList());
                return result;
            }

            var signatories = await _signatoryRepositoryReader.All();

            var contractSignaturesResult = await _contractService.GetContractSignaturesByLawsuitPartiesAsync(request.PlaintiffSignatures.ToArray(), request.DefendantSignatures.ToArray(), signatories);
            if (contractSignaturesResult.Code == ResultCodeEnum.Ok)
            {
                var signaturesRequiredsResult = await _signatoryService.GetSignatoriesRequiredToWinByLawsuitParty(contractSignaturesResult.Data, signatories);
                if (signaturesRequiredsResult.Code == ResultCodeEnum.Ok)
                {
                    string plaintiffSignaturedRequiredToWin = string.Empty, defendantSignatureRequiredToWin = string.Empty;
                    signaturesRequiredsResult.Data.ToList().ForEach(l =>
                    {
                        if (!(l.Value == null))
                        {
                            result.AppendMessage($"{l.Key.Name} require minimum the ({l.Value?.Key}) signature to win the trial");
                            if (l.Key == LawsuitParty.Plaintiff)
                                plaintiffSignaturedRequiredToWin = l.Value.Key.ToString();
                            else if (l.Key == LawsuitParty.Defendant)
                                defendantSignatureRequiredToWin = l.Value.Key.ToString();
                        }
                    });
                    result.Data = new DetermineSignatureRequiredToWinQueryResult(plaintiffSignaturedRequiredToWin, defendantSignatureRequiredToWin);

                    if (string.IsNullOrEmpty(plaintiffSignaturedRequiredToWin) && string.IsNullOrEmpty(defendantSignatureRequiredToWin))
                        result.AppendMessage("No combinations of signatures were found to determine winners");
                }
            }
            else
                result.AppendError("An error occurred while determining the signatures required to win");
        }
        catch (LawsuitsDomainException e)
        {
            result.AppendError(e);
        }
        return result;
    }
    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
            }
            _disposed = true;
        }
    }
    ~DetermineSignatureRequiredToWinQueryHandler() { Dispose(disposing: false); }
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
