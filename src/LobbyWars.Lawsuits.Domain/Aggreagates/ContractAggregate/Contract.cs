﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;

/// <summary>
/// Represent a lawsuit contract
/// </summary>
public class Contract : Entity, IAggregateRoot, IContractAggregate
{
    private readonly List<ContractSignature> _signatures;
    /// <summary>
    /// Trial number
    /// </summary>
    /// <example>TRIAL-08062023-A0001</example>
    public string TrialNumber { get; set; }
    public Contract() { }

    public Contract(string trialNumber)
    {
        TrialNumber = trialNumber ?? throw new ArgumentNullException(nameof(trialNumber));
        _signatures = new List<ContractSignature>();
    }
    /// <summary>
    /// Contract signatures
    /// </summary>
    /// <remarks>
    ///   Signatures of contract based on lawsuit party
    /// </remarks>
    public IReadOnlyCollection<ContractSignature> Signatures => _signatures;
    /// <summary>
    /// Add a new signature based on a signatory 
    /// </summary>
    /// <param name="lawsuitParty">Lawsuit party <see cref="LawsuitParty"/></param>
    /// <param name="signatory">Signatory<see cref="Signatory"/></param>
    public void AddSignature(LawsuitParty lawsuitParty, Signatory signatory) => _signatures.Add(new ContractSignature(Id, signatory.Id, lawsuitParty));
    /// <summary>
    /// Add new signatures
    /// </summary>
    /// <param name="signatures">Signatories <see cref="ContractSignature"/></param>
    public void AddSignatures(IEnumerable<ContractSignature> signatures) => _signatures.AddRange(signatures);
}
