﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;

/// <summary>
/// Represent a contract signature
/// </summary>
public class ContractSignature : Entity, IContractAggregate
{
    public ContractSignature() { }
    public ContractSignature(Guid contractId, Guid signatoryId, LawsuitParty lawsuitParty)
    {
        ContractId = contractId;
        SignatoryId = signatoryId;
        LawsuitParty = lawsuitParty ?? throw new ArgumentNullException(nameof(lawsuitParty));
    }
    /// <summary>
    /// Contract identifier
    /// </summary>
    public Guid ContractId { get; private set; }
    /// <summary>
    /// Contract
    /// </summary>
    public Contract Contract { get; private set; }
    /// <summary>
    ///  Signatory identifier
    /// </summary>
    public Guid SignatoryId { get; private set; }
    /// <summary>
    /// Lawsuit party
    /// </summary>
    public LawsuitParty LawsuitParty { get; private set; }

}
