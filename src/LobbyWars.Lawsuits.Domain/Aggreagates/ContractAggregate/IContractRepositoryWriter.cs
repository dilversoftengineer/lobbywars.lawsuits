﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;

/// <summary>
/// Contract repository writer
/// </summary>
public interface IContractRepositoryWriter : IRepository<Contract>, IDisposable
{   
    /// <summary>
    /// Add new contract to repository
    /// </summary>
    /// <param name="contract">Contract</param>
    Task<bool> AddAsync(Contract contract);
}
