﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;

public interface ITrialVereditRepositoryWriter : IRepository<Contract>
{
    /// <summary>
    /// Add new veredit to repository
    /// </summary>
    /// <param name="veredict">Trial Veredict</param>
    Task<bool> AddAsync(TrialVeredict veredict);
}
