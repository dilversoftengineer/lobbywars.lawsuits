﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;

/// <summary>
/// Represent a lawsuit party
/// </summary>
public class LawsuitParty : Enumeration
{
    public static LawsuitParty Plaintiff = new(1, nameof(Plaintiff));
    public static LawsuitParty Defendant = new(2, nameof(Defendant));
    public LawsuitParty(int id, string name) : base(id, name)
    {
    }
}
