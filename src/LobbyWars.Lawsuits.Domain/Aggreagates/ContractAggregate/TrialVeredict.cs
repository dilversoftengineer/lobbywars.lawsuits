﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;

/// <summary>
/// Represent a trial veredict
/// </summary>
public class TrialVeredict : Entity, IAggregateRoot
{
    public TrialVeredict(){ }

    public TrialVeredict(string lawsuitPartyWinner, short points, string trialNumber, string judge = "")
    {
        LawsuitPartyWinner = lawsuitPartyWinner ?? throw new ArgumentNullException(nameof(lawsuitPartyWinner));
        Points = points;
        TrialNumber = trialNumber ?? throw new ArgumentNullException(nameof(trialNumber));
        Judge = judge ;
    }
    /// <summary>
    /// Lawsuit party winner
    /// </summary>
    public string LawsuitPartyWinner { get; private set; }
    /// <summary>
    /// Point of lawsuit party winner
    /// </summary>
    public short Points { get; private set; }
    /// <summary>
    /// Trial number
    /// </summary>
    public string TrialNumber { get; private set; }
    /// <summary>
    /// Judge identifier who pronounced the verdict
    /// </summary>
    public string? Judge { get; private set; }

}
