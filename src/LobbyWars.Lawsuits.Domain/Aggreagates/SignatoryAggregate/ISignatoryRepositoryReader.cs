﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.SignatoryAggregate;

/// <summary>
/// Signatories repository reader
/// </summary>
public interface ISignatoryRepositoryReader 
{
    /// <summary>
    /// Get all signatories
    /// </summary>
    /// <returns><see cref="IReadOnlyCollection<Signatory>"/></returns>
    Task<IReadOnlyCollection<Signatory>> All();
}
