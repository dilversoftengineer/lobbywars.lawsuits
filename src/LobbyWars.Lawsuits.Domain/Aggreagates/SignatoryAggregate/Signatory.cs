﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.SignatoryAggregate;

/// <summary>
/// Contract signer
/// </summary>
public class Signatory : Entity, IAggregateRoot
{
    public Signatory() { }
    public Signatory(string name, SignatoryRoleKey key)
    {
        Name = name ?? throw new ArgumentNullException(nameof(name));
        Points = (short)key.Id;
        Key = key;
    }
    /// <summary>
    /// Signer name
    /// </summary>
    /// <example>King</example>
    public string Name { get; set; }
    /// <summary>
    /// Key
    /// </summary>
    public SignatoryRoleKey Key { get; set; }
    /// <summary>
    /// Points 
    /// </summary>
    public short Points { get; set; }
}
