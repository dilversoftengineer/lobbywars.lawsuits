﻿namespace LobbyWars.Lawsuits.Domain.Aggreagates.SignatoryAggregate;

/// <summary>
/// Represent signatory role key
/// </summary>
public class SignatoryRoleKey : Enumeration
{
    public static SignatoryRoleKey K = new(5, nameof(K));
    public static SignatoryRoleKey N = new(2, nameof(N));
    public static SignatoryRoleKey V = new(1, nameof(V));
    public static SignatoryRoleKey Valueless = new(0, "#");
    public SignatoryRoleKey(int id, string name) : base(id, name)
    {

    }
}
