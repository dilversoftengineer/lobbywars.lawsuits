﻿namespace LobbyWars.Lawsuits.Domain.Exceptions;

public class LawsuitsDomainException : Exception
{
    public LawsuitsDomainException()
    { }

    public LawsuitsDomainException(string message)
        : base(message)
    { }

    public LawsuitsDomainException(string message, Exception innerException)
        : base(message, innerException)
    { }
}
