﻿namespace LobbyWars.Lawsuits.Domain.SeedWork;
public interface IUnitOfWork : IDisposable
{
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    /// <summary>
    /// Save changes
    /// </summary>
    /// <remarks>Validate if this transaction has been executed successfully</remarks>
    /// <returns>confirmation of successful transaction</returns>
    Task<bool> SaveEntityChangesAsync(CancellationToken cancellationToken = default);
}
