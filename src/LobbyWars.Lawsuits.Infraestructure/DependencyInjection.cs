﻿namespace LobbyWars.Lawsuits.Infraestructure;
public static class DependencyInjection
{
    public static IServiceCollection AddInfraestructureServices(this IServiceCollection services)
    {
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        services.AddScoped<ISignatoryRepositoryReader, SignatoryRepositoryReader>();
        services.AddScoped<IContractRepositoryWriter, ContractRepositoryWriter>();
        services.AddScoped<ITrialVereditRepositoryWriter, TrialVereditRepositoryWriter>();
        services.AddScoped<IApplicationMapper, ApplicationAutoMapper>();
        services.AddScoped<IValidator<DetermineWinnerCommand>, DetermineWinnerCommandValidator>();
        return services;
    }
}
