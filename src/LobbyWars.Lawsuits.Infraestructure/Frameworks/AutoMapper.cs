﻿namespace LobbyWars.Lawsuits.Infraestructure.Frameworks
{
    /// <summary>
    /// Auto mapper a entity to a business model or data transfer object
    /// </summary>
    public class ApplicationAutoMapper : IApplicationMapper
    {
        private readonly IMapper _mapper;
        public ApplicationAutoMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        /// <summary>
        ///  Map entity to model using AutoMapper
        /// </summary>
        /// <typeparam name="I">source type</typeparam>
        /// <param name="source">object to map</param>
        public async Task<I> Map<I>(object source)
            where I : class => await Task.FromResult(_mapper.Map<I>(source));

    }

    /// <summary>
    /// Auto Mapper Profile
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<GetAllSignatoriesQueryResult, Signatory>().ReverseMap();
        }
    }
}
