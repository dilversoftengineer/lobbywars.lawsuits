﻿namespace LobbyWars.Lawsuits.Infraestructure.Frameworks
{
    public class DetermineWinnerCommandValidator : AbstractValidator<DetermineWinnerCommand>
    {
        private static readonly char[] _validSigningkeys = Enumeration.GetAll<SignatoryRoleKey>().Select(k => k.Name.ToUpper().First()).ToArray();
        public DetermineWinnerCommandValidator()
        {
            string invalidSignaturesMessage = $"has invalid signatures. Valid Signing Keys ({new string(_validSigningkeys)}#)";
            RuleFor(command => command.TrialNumber).NotEmpty();
            RuleFor(command => command.DefendantSignatures).NotEmpty().MaximumLength(3).WithMessage("DefendantSignatures maximum length is 3");
            RuleFor(command => command.PlaintiffSignatures).NotEmpty().MaximumLength(3).WithMessage("PlaintiffSignatures maximum length is 3");
            RuleFor(command => command.DefendantSignatures).Must(ValidateSignature).WithMessage($"DefendantSignatures {invalidSignaturesMessage}");
            RuleFor(command => command.PlaintiffSignatures).Must(ValidateSignature).WithMessage($"PlaintiffSignatures {invalidSignaturesMessage}");
        }

        public static bool ValidateSignature(string signature)
        {
            foreach (char key in signature.Select(char.ToUpper))
                if (key != SignatoryRoleKey.Valueless.Name.First() && !_validSigningkeys.Contains(key)) return false;
            return true;
        }
    }

}
