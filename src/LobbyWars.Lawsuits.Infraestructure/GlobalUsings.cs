﻿global using global::LobbyWars.Lawsuits.Domain.Exceptions;
global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Reflection;
global using System.Threading.Tasks;
global using System.Threading;
global using System.Data;
global using Microsoft.Data.SqlClient;
global using Microsoft.Extensions.Configuration;
global using Microsoft.Extensions.Logging;
global using Microsoft.Extensions.DependencyInjection;


///projects
global using LobbyWars.Lawsuits.Application;
global using LobbyWars.Lawsuits.Domain.Aggreagates.SignatoryAggregate;
global using LobbyWars.Lawsuits.Domain.SeedWork;
global using LobbyWars.Lawsuits.Application.UseCases.Signatory.Queries;
global using LobbyWars.Lawsuits.Application.SeedWork;
global using LobbyWars.Lawsuits.Application.UseCases.Contract.Commands;
global using LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;
global using LobbyWars.Lawsuits.Infraestructure.Frameworks;
global using LobbyWars.Lawsuits.Infraestructure.Repositories;

///frameworks
global using AutoMapper;
global using IMapper = AutoMapper.IMapper;
global using Dapper;
global using Microsoft.EntityFrameworkCore;
global using FluentValidation;

