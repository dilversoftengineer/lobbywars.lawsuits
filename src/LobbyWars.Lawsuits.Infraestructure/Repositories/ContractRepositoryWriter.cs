﻿using LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;

namespace LobbyWars.Lawsuits.Infraestructure.Repositories
{
    public class ContractRepositoryWriter : IContractRepositoryWriter
    {
        private readonly LobbyWarsLawsuitsContext _lobbyWarsLawsuitsContext;

        public ContractRepositoryWriter(LobbyWarsLawsuitsContext lobbyWarsLawsuitsContext)
        {
            _lobbyWarsLawsuitsContext = lobbyWarsLawsuitsContext;
        }

        public IUnitOfWork UnitOfWork => _lobbyWarsLawsuitsContext;

        public async Task<bool> AddAsync(Contract contract)
        {
            Contract? existingContract = await _lobbyWarsLawsuitsContext.Contract.FirstOrDefaultAsync(v => v.TrialNumber == contract.TrialNumber);
            if (existingContract != null)
                _lobbyWarsLawsuitsContext.Contract.Remove(existingContract);

            await _lobbyWarsLawsuitsContext.Contract.AddAsync(contract);
            return true;
        }

        public void Dispose()
        {
            _lobbyWarsLawsuitsContext.Dispose();
        }
    }
}
