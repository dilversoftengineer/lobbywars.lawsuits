﻿namespace LobbyWars.Lawsuits.Infraestructure.Repositories;

public class LobbyWarsLawsuitsContext : DbContext, IUnitOfWork
{
    private readonly ILogger<LobbyWarsLawsuitsContext> _logger;

    public LobbyWarsLawsuitsContext(DbContextOptions<LobbyWarsLawsuitsContext> options, ILogger<LobbyWarsLawsuitsContext> logger)
        : base(options)
    {
        _logger = logger;
    }
    public DbSet<Signatory> Signatory { get; set; }
    public DbSet<Contract> Contract { get; set; }
    public DbSet<ContractSignature> ContractSignature { get; set; }
    public DbSet<TrialVeredict> TrialVeredict { get; set; }


    public async Task<bool> SaveEntityChangesAsync(CancellationToken cancellationToken = default)
    {
        try
        {
            await base.SaveChangesAsync(cancellationToken);
            return true;
        }
        catch (DbUpdateException e)
        {
            _logger.LogError(e, e.Message);
            return false;
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Signatory>()
            .Property(c => c.Key)
           .HasConversion(
             l => l.Name,
                 l => Enumeration.FromDisplayName<SignatoryRoleKey>(l));

        modelBuilder.Entity<Signatory>().HasData(
             new Signatory("King", SignatoryRoleKey.K),
             new Signatory("Notary", SignatoryRoleKey.N),
             new Signatory("Validator", SignatoryRoleKey.V)
        );

        modelBuilder.Entity<Contract>()
              .HasMany(c => c.Signatures)
              .WithOne(s => s.Contract)
              .HasForeignKey(e => e.ContractId)
              .IsRequired();

        modelBuilder.Entity<ContractSignature>()
            .Property(c => c.LawsuitParty)
           .HasConversion(
             l => l.Name,
                 l => Enumeration.FromDisplayName<LawsuitParty>(l));


        modelBuilder.Entity<Contract>()
              .HasIndex(b => b.TrialNumber);

        //modelBuilder.Entity<LawsuitParty>()
        //    .HasKey(o => o.Id);
        //modelBuilder.Entity<LawsuitParty>().Property(o => o.Id)
        //    .ValueGeneratedNever()
        //    .IsRequired();
        //modelBuilder.Entity<LawsuitParty>().Property(o => o.Name)
        //    .HasMaxLength(200)
        //    .IsRequired();

        modelBuilder.Entity<TrialVeredict>()
              .HasIndex(b => b.TrialNumber)
              .IsUnique();

    }

}
