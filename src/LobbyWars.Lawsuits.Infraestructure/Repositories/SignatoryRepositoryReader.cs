﻿using System.Diagnostics.Metrics;
using static System.Diagnostics.Activity;

namespace LobbyWars.Lawsuits.Infraestructure.Repositories
{
    public class SignatoryRepositoryReader : ISignatoryRepositoryReader
    {
        private readonly ILogger<SignatoryRepositoryReader> _logger;
        private readonly IConfiguration _configuration;
        private readonly static string S_CONNECTION_STRING_NAME = "LobbyWarsLawsuits";
        public SignatoryRepositoryReader(ILogger<SignatoryRepositoryReader> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public async Task<IReadOnlyCollection<Signatory>> All()
        {
            try
            {
                using IDbConnection connection = CreateConnection();
                connection.Open();
                SqlMapper.AddTypeHandler(typeof(SignatoryRoleKey), new SignatoryRoleKeyHandler());
                return (IReadOnlyCollection<Signatory>)await connection.QueryAsync<Signatory>($"SELECT [Id], [Name], [Key], [Points] FROM  Signatory");
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }
        public IDbConnection CreateConnection()
                   => new SqlConnection(_configuration.GetConnectionString(S_CONNECTION_STRING_NAME));


        public class SignatoryRoleKeyHandler : SqlMapper.ITypeHandler
        {
            public object Parse(Type destinationType, object value)
            {
                if (destinationType == typeof(SignatoryRoleKey))
                    return Enumeration.FromDisplayName<SignatoryRoleKey>(value.ToString());
                else return null;
            }

            public void SetValue(IDbDataParameter parameter, object value)
            {
                parameter.DbType = DbType.String;
                parameter.Value = (string)(dynamic)value;
            }
        }
    }
}
