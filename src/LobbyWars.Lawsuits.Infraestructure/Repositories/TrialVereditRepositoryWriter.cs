﻿using LobbyWars.Lawsuits.Domain.Aggreagates.ContractAggregate;

namespace LobbyWars.Lawsuits.Infraestructure.Repositories
{
    public class TrialVereditRepositoryWriter : ITrialVereditRepositoryWriter
    {
        private readonly LobbyWarsLawsuitsContext _lobbyWarsLawsuitsContext;

        public TrialVereditRepositoryWriter(LobbyWarsLawsuitsContext lobbyWarsLawsuitsContext)
        {
            _lobbyWarsLawsuitsContext = lobbyWarsLawsuitsContext;
        }

        public IUnitOfWork UnitOfWork => _lobbyWarsLawsuitsContext;

        public async Task<bool> AddAsync(TrialVeredict veredict)
        {
            TrialVeredict? existingVeredit = await _lobbyWarsLawsuitsContext.TrialVeredict.FirstOrDefaultAsync(v=> v.TrialNumber == veredict.TrialNumber);
            if (existingVeredit != null)
                _lobbyWarsLawsuitsContext.TrialVeredict.Remove(existingVeredit);

            await _lobbyWarsLawsuitsContext.TrialVeredict.AddAsync(veredict);
            return true;
        }
    }
}
