﻿using Microsoft.AspNetCore.Builder;

namespace LobbyWars.Lawsuits.Infraestructure
{
    public static class WebApiBuilder
    {
        public static WebApplicationBuilder CreateWebApplicationBuilder(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            Application.DependencyInjection.AddApplicationServices(builder.Services);
            return builder;
        }
    }
}
