﻿namespace LobbyWars.Lawsuits.WebApi.Endpoints;
public static class Endpoints
{
    public static WebApplication Register(this WebApplication app)
    {
        SignatoriesEndpoints.Register(app);
        LawsuitsEndpoints.Register(app);
        return app;
    }
}
