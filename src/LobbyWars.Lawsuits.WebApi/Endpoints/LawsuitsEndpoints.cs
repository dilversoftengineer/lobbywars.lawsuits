﻿using LobbyWars.Lawsuits.Application.Models;
using Microsoft.AspNetCore.Authorization;

namespace LobbyWars.Lawsuits.WebApi.Endpoints;

public static class LawsuitsEndpoints
{
    public static WebApplication Register(this WebApplication app)
    {
        app.MapGroup("/api")
           .MapGroup("/lawsuitveredicts")
           .MapPost(string.Empty, [Authorize] async (IMediator mediator, IValidator<DetermineWinnerCommand> validator, DetermineWinnerCommand request) =>
           {
               FluentValidation.Results.ValidationResult validation = await validator.ValidateAsync(request);
               if (!validation.IsValid)
                   return Results.ValidationProblem(validation.ToDictionary());

               var result = await mediator.Send(request);

               if (result.Code == Application.SeedWork.ResultCodeEnum.Ok)
                   return Results.Ok(await mediator.Send(request));

               if (result.Code == Application.SeedWork.ResultCodeEnum.Error)
                   return Results.Problem(detail: result.Errors.FirstOrDefault(), statusCode: 500);

               return Results.Ok();
           })
           .Produces<Result<DetermineWinnerCommandResult>>();

        return app;
    }
}
