﻿using LobbyWars.Lawsuits.Application.Models;
using LobbyWars.Lawsuits.Application.UseCases.Signatory.Queries;
using LobbyWars.Lawsuits.Application.UseCases.SignatureRequiredToWin.Commands;
using Microsoft.AspNetCore.Authorization;

namespace LobbyWars.Lawsuits.WebApi.Endpoints
{
    public static class SignatoriesEndpoints
    {
        public static WebApplication Register(this WebApplication app)
        {
            app.MapGroup("/api")
               .MapGroup("/signatories")
               .MapGet("", [Authorize] (IMediator mediator) => mediator.Send(new GetAllSignatoriesQuery()));

            app.MapGroup("/api")
                .MapGroup("/signaturerequiredtowin")
                .MapGet("", [Authorize] async (IMediator mediator, string PlaintiffSignatures, string DefendantSignatures) =>
                {
                    var result = await mediator.Send(new DetermineSignatureRequiredToWinQuery(PlaintiffSignatures, DefendantSignatures));
                    if (result.Code == Application.SeedWork.ResultCodeEnum.BadRequest)
                        return Results.BadRequest(result);

                    if (result.Code == Application.SeedWork.ResultCodeEnum.Ok)
                        return Results.Ok(result);

                    if (result.Code == Application.SeedWork.ResultCodeEnum.Error)
                        return Results.Problem(detail: result.Errors.FirstOrDefault(), statusCode: 500);

                    return Results.Ok();
                })
                .Produces<Result<DetermineSignatureRequiredToWinQueryResult>>();

            return app;
        }
    }
}
