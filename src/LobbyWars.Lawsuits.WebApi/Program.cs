WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

LobbyWars.Lawsuits.Infraestructure.DependencyInjection.AddInfraestructureServices(builder.Services);
LobbyWars.Lawsuits.Application.DependencyInjection.AddApplicationServices(builder.Services);

builder.Services.AddDbContext<LobbyWarsLawsuitsContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("LobbyWarsLawsuits")));

var securityScheme = new OpenApiSecurityScheme()
{
    Name = "Authorization",
    Type = SecuritySchemeType.ApiKey,
    Scheme = "Bearer",
    BearerFormat = "JWT",
    In = ParameterLocation.Header,
    Description = "JSON Web Token based security",
};

var securityReq = new OpenApiSecurityRequirement()
{
    {
        new OpenApiSecurityScheme
        {
            Reference = new OpenApiReference
            {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
            }
        },
        Array.Empty<string>()
    }
};

var contact = new OpenApiContact()
{
    Name = "Dilver Javier Valencia Endo",
    Email = "dilvertsoft@gmail.com",
    Url = new Uri("https://www.linkedin.com/in/dilvervalencia/")
};

var license = new OpenApiLicense()
{
    Name = "Free License",
    Url = new Uri("https://www.linkedin.com/in/dilvervalencia/")
};

var info = new OpenApiInfo()
{
    Version = "v1",
    Title = "Lobby Wars Api",
    Description = "We are in the era of \"lawsuits\", everyone wants to go to court with their lawyer Saul and try to get a lot of dollars as if they were raining over\r\nManhattan.\r\nThe laws have changed much lately and governments have been digitized. That's when Signaturit comes into play.\r\nThe city council through the use of Signaturit maintains a registry of legal signatures of each party involved in the contracts that are made.\r\nDuring a trial, justice only verifies the signatures of the parties involved in the contract to decide who wins. For that, they assign points to the\r\ndifferent signatures depending on the role of who signed.\r\nFor example, if the plaintiff has a contract that is signed by a notary he gets 2 points, if the defendant has in the contract the signature of\r\n only a v\r\nalidator he gets only 1 point, so the plaintiff party wins the trial.",
    TermsOfService = new Uri("http://www.example.com"),
    Contact = contact,
    License = license
};

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(o =>
{
    o.SwaggerDoc("v1", info);
    o.AddSecurityDefinition("Bearer", securityScheme);
    o.AddSecurityRequirement(securityReq);
});



// Add JWT configuration
builder.Services.AddAuthentication(o =>
{
    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    o.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(o =>
{
    o.TokenValidationParameters = new TokenValidationParameters
    {
        ValidIssuer = builder.Configuration["Jwt:Issuer"],
        ValidAudience = builder.Configuration["Jwt:Audience"],
        IssuerSigningKey = new SymmetricSecurityKey
            (Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])),
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = false,
        ValidateIssuerSigningKey = true
};
});

builder.Services.AddAuthorization();
builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();

using (IServiceScope serviceScope = app.Services.GetService<IServiceScopeFactory>().CreateScope())
{
    await serviceScope.ServiceProvider.GetRequiredService<LobbyWarsLawsuitsContext>().Database.EnsureCreatedAsync();
    await serviceScope.ServiceProvider.GetRequiredService<LobbyWarsLawsuitsContext>().Database.MigrateAsync();
}

app.UseSwagger();
app.UseSwaggerUI();
app.UseAuthentication();
app.UseAuthorization();
//app.UseHttpsRedirection();

TokenEndpoint.Add(app, builder.Configuration);
Endpoints.Register(app);



app.Run();

record UserDto(string UserName, string Password);