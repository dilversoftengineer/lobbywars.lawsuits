﻿using System.Collections.Generic;

namespace LobbyWars.Lawsuits.Application.UnitTests.Services;
public class ContractServiceTests
{
    #region DetermineLawsuitPartyWinner
    [Theory]
    [MemberData(nameof(DetermineLawsuitPartyWinner_winner_Cases))]
    public static async Task DetermineLawsuitPartyWinner_get_winner(LawsuitParty partyWinnerExpected, List<ContractSignatureModel> contractSignatures)
    {
        var result = await DetermineLawsuitPartyWinner("CONTR-08062023-A0001", contractSignatures);
        Assert.Equal(partyWinnerExpected.Name, result.Data.LawsuitPartyWinner);
    }

    [Theory]
    [MemberData(nameof(DetermineLawsuitPartyWinner_loser_contain_king_and_validator_Cases))]
    public static async Task DetermineLawsuitPartyWinner_loser_contain_king_and_validator(LawsuitParty partyWinnerExpected, List<ContractSignatureModel> contractSignatures)
    {
        var result = await DetermineLawsuitPartyWinner("CONTR-08062023-A0001", contractSignatures);
        Assert.Equal(partyWinnerExpected?.Name, result.Data?.LawsuitPartyWinner);
    }
    public static async Task<Result<TrialVeredictResult>> DetermineLawsuitPartyWinner(string trialNumber, IReadOnlyCollection<ContractSignatureModel> contractSignatures)
    {
        var logger = Mock.Of<ILogger<ContractService>>();

        return await new ContractService(logger, new SignatoryService(Mock.Of<ILogger<SignatoryService>>()))
            .DetermineLawsuitPartyWinner(trialNumber, contractSignatures);
    }

    public static IEnumerable<object[]> DetermineLawsuitPartyWinner_winner_Cases => new List<object[]>
    {
        new object[] { LawsuitParty.Plaintiff, new List<ContractSignatureModel>()
            {
                new(new ("King", SignatoryRoleKey.K), LawsuitParty.Plaintiff),
                new(new ("Notary", SignatoryRoleKey.N), LawsuitParty.Plaintiff),
                new(new ("Notary", SignatoryRoleKey.N), LawsuitParty.Defendant),
                new(new ("Notary", SignatoryRoleKey.N), LawsuitParty.Defendant),
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Defendant)
            }
        },
        new object[] { LawsuitParty.Defendant, new List<ContractSignatureModel>()
            {
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Plaintiff),
                new(new ("Notary", SignatoryRoleKey.N), LawsuitParty.Plaintiff),
                new(new ("Notary", SignatoryRoleKey.N), LawsuitParty.Defendant),
                new(new ("Notary", SignatoryRoleKey.N), LawsuitParty.Defendant),
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Defendant)
            }
        }
    };
    public static IEnumerable<object[]> DetermineLawsuitPartyWinner_loser_contain_king_and_validator_Cases => new List<object[]>
    {
        new object[] { LawsuitParty.Plaintiff, new List<ContractSignatureModel>()
            {
                new(new ("King", SignatoryRoleKey.K), LawsuitParty.Plaintiff),
                new(new ("Notary", SignatoryRoleKey.N), LawsuitParty.Plaintiff),
                new(new ("King", SignatoryRoleKey.N), LawsuitParty.Defendant),
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Defendant),
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Defendant)
            }
        },
        new object[] { LawsuitParty.Defendant, new List<ContractSignatureModel>()
            {
                new(new ("King", SignatoryRoleKey.K), LawsuitParty.Plaintiff),
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Plaintiff),
                new(new ("King", SignatoryRoleKey.K), LawsuitParty.Defendant),
                new(new ("Notary", SignatoryRoleKey.N), LawsuitParty.Defendant),
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Defendant)
            }
        },
        new object[] { new LawsuitParty(0,string.Empty), new List<ContractSignatureModel>()
            {
                new(new ("King", SignatoryRoleKey.K), LawsuitParty.Plaintiff),
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Plaintiff),
                new(new ("King", SignatoryRoleKey.K), LawsuitParty.Defendant),
                new(new ("Validator", SignatoryRoleKey.V), LawsuitParty.Defendant)
            }
        },
    };
    #endregion


    [Theory]
    [MemberData(nameof(GetContractSignaturesByKeysAsync_cases))]
    public static async Task GetContractSignaturesByKeysAsync_base(LawsuitParty plaintiff, char[] signaturesKeys)
    {
        List<Signatory> signatories = new()
        {
            new Signatory("King", SignatoryRoleKey.K),
            new Signatory("Notary", SignatoryRoleKey.N),
            new Signatory("Validator", SignatoryRoleKey.V)
        };
        var result = await GetContractSignaturesByKeysAsync(signaturesKeys, plaintiff, signatories);
        bool equal = result.Data?.Select(l => l.Signatory.Key.Name.First()).ToArray().ToString() == signaturesKeys.ToString();
        Assert.True(equal);
    }
    [Theory]
    [MemberData(nameof(GetContractSignaturesByLawsuitPartiesAsync_base_Cases))]
    public async Task GetContractSignaturesByLawsuitPartiesAsync_base(short quantityExpected, char[] plaintiffSignatures, char[] defendantSignatures)
    {
        List<Signatory> signatories = new()
        {
            new Signatory("King", SignatoryRoleKey.K),
            new Signatory("Notary", SignatoryRoleKey.N),
            new Signatory("Validator", SignatoryRoleKey.V)
        };
        var result = await GetContractSignaturesByLawsuitPartiesAsync(plaintiffSignatures, defendantSignatures, signatories);
        Assert.Equal(quantityExpected,result.Data?.Count());
    }


    public static async Task<Result<IEnumerable<ContractSignatureModel>>> GetContractSignaturesByKeysAsync(char[] signaturesKeys, LawsuitParty lawsuitParty, IReadOnlyCollection<Signatory> signatories)
        => await new ContractService(Mock.Of<ILogger<ContractService>>(), new SignatoryService(Mock.Of<ILogger<SignatoryService>>()))
            .GetContractSignaturesByKeysAsync(signaturesKeys, lawsuitParty, signatories);

    public async Task<Result<IEnumerable<ContractSignatureModel>>> GetContractSignaturesByLawsuitPartiesAsync(char[] plaintiffSignatures, char[] defendantSignatures, IReadOnlyCollection<Signatory> signatories)
    => await new ContractService(Mock.Of<ILogger<ContractService>>(), new SignatoryService(Mock.Of<ILogger<SignatoryService>>()))
        .GetContractSignaturesByLawsuitPartiesAsync(plaintiffSignatures, defendantSignatures, signatories);

    public static IEnumerable<object[]> GetContractSignaturesByKeysAsync_cases => new List<object[]>
    {
        new object[] {LawsuitParty.Plaintiff, new char[]{ 'V', 'N'}},
        new object[] { LawsuitParty.Plaintiff, new char[]{ 'K', 'N'}},
        new object[] { LawsuitParty.Plaintiff, new char[]{ 'V', 'K'}},
        new object[] { LawsuitParty.Plaintiff, new char[]{ 'V', 'V'}},
        new object[] {LawsuitParty.Plaintiff, new char[]{ 'V', 'N'}},
        new object[] { LawsuitParty.Defendant, new char[]{ 'K', 'N'}},
        new object[] { LawsuitParty.Defendant, new char[]{ 'V', 'K'}}
    };

    public static IEnumerable<object[]> GetContractSignaturesByLawsuitPartiesAsync_base_Cases => new List<object[]>
    {
        new object[] {4, new char[]{ 'V', 'N'}, new char[]{ 'K', 'V'}},
        new object[] {3, new char[]{ 'N', 'N'}, new char[]{ 'K', '#' } }
    };
}