﻿namespace LobbyWars.Lawsuits.Application.UnitTests.Services;
public sealed class SignatoryServiceTests
{
    #region SumPoints

    [Theory]
    [MemberData(nameof(SumPoints_without_business_rules_Cases))]
    public static async Task SumPoints_without_business_rules(short pointsExpecteds, IEnumerable<Signatory> signers)
    {
        var result = await SumPoints(signers);
        Assert.Equal(pointsExpecteds, result.Data);
    }

    [Theory]
    [MemberData(nameof(SumPoints_with_king_and_validator_Cases))]
    public static async Task SumPoints_with_king_and_validator(short pointsExpecteds, IEnumerable<Signatory> signers)
    {
        var result = await SumPoints(signers);
        Assert.Equal(pointsExpecteds, result.Data);
    }
    public static async Task<Result<short>> SumPoints(IEnumerable<Signatory> signers)
    => await new SignatoryService(Mock.Of<ILogger<SignatoryService>>()).SumPoints(signers);



    #region Cases
    public static IEnumerable<object[]> SumPoints_without_business_rules_Cases => new List<object[]>
                {
                    new object[] { 15, new List<Signatory>()
                        {
                            new ("King", SignatoryRoleKey.K),
                            new ("King", SignatoryRoleKey.K),
                            new ("King", SignatoryRoleKey.K)
                        }
                    },
                    new object[] { 7, new List<Signatory>()
                        {
                            new ("King", SignatoryRoleKey.K),
                            new ("Notary", SignatoryRoleKey.N)
                        }
                    },
                };

    public static IEnumerable<object[]> SumPoints_with_king_and_validator_Cases => new List<object[]>
                {
                    new object[] { 7, new List<Signatory>()
                        {
                            new ("King", SignatoryRoleKey.K),
                            new ("Validator", SignatoryRoleKey.V),
                            new ("Notary", SignatoryRoleKey.N)
                        }
                    },
                    new object[] {5, new List<Signatory>()
                        {
                            new ("King", SignatoryRoleKey.K),
                            new ("Validator", SignatoryRoleKey.V)
                        }
                    },
                };
    #endregion
    #endregion

    [Fact]
    public static async Task GetSignatoriesByKeys_map_all()
    {
        char[] signatureKeys = { 'K', 'N', 'V' };
        List<Signatory> signatories = new()
        {
            new Signatory("King", SignatoryRoleKey.K),
            new Signatory("Notary", SignatoryRoleKey.N),
            new Signatory("Validator", SignatoryRoleKey.V)
        };
        var result = await GetSignatoriesByKeys(signatureKeys, signatories);

        Assert.Equal(signatories, result.Data);
    }
    public static async Task<Result<IReadOnlyCollection<Signatory>>> GetSignatoriesByKeys(char[] signatureKeys, IReadOnlyCollection<Signatory> signatories)
        => await new SignatoryService(Mock.Of<ILogger<SignatoryService>>()).GetSignatoriesByKeys(signatureKeys, signatories);

}