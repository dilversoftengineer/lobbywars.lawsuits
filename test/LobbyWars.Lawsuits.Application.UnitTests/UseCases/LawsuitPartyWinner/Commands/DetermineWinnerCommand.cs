﻿namespace LobbyWars.Lawsuits.Application.UseCases.Contract.Commands;
public class DetermineWinnerCommandHandlerTests
{
    [Theory]
    [MemberData(nameof(Determine_lawsuit_party_winner_Cases))]
    public static async Task Determine_lawsuit_party_winner(string lawsuitPartyWinnerExpected, DetermineWinnerCommand request)
    {
        var result = await Handle(request);
        Assert.Equal(lawsuitPartyWinnerExpected, result.Data?.LawsuitPartyWinner);
    }

    public static async Task<Result<DetermineWinnerCommandResult>> Handle(DetermineWinnerCommand request)
    {
        var _signatoryService = new SignatoryService(Mock.Of<ILogger<SignatoryService>>());
        var _contractService = new ContractService(Mock.Of<ILogger<ContractService>>(),_signatoryService);
        var signatories = await GetSignatories();
        var _signatoryRepositoryReader = new Mock<ISignatoryRepositoryReader>();
        _signatoryRepositoryReader.Setup(l=> l.All()).Returns(Task.FromResult(signatories));

        var _contractRepositoryWriter = new Mock<IContractRepositoryWriter>();
        _contractRepositoryWriter.Setup(l=> l.AddAsync(new Domain.Aggreagates.ContractAggregate.Contract())).Returns(Task.FromResult(true));
        _contractRepositoryWriter.Setup(l=> l.UnitOfWork.SaveEntityChangesAsync(default)).Returns(Task.FromResult(true));

        var _trialVereditRepositoryWriter = new Mock<ITrialVereditRepositoryWriter>();
        _trialVereditRepositoryWriter.Setup(l=> l.AddAsync(new TrialVeredict())).Returns(Task.FromResult(true));
        _trialVereditRepositoryWriter.Setup(l => l.UnitOfWork.SaveEntityChangesAsync(default)).Returns(Task.FromResult(true));

        return await new DetermineWinnerCommandHandler(Mock.Of<ILogger<DetermineWinnerCommandHandler>>(),
            _contractRepositoryWriter.Object,
            _signatoryRepositoryReader.Object,
            _signatoryService,
            _contractService,
            _trialVereditRepositoryWriter.Object)
            .Handle(request, default);
    }

    public static IEnumerable<object[]> Determine_lawsuit_party_winner_Cases => new List<object[]>
                {
                    new object[] { LawsuitParty.Plaintiff.Name, new DetermineWinnerCommand("CONTR-08062023-A0001","KN", "NNV") },
                    new object[] { LawsuitParty.Defendant.Name, new DetermineWinnerCommand("CONTR-08062023-A0001","KVV", "NNK") },
                    new object[] { string.Empty, new DetermineWinnerCommand("CONTR-08062023-A0001","KKK", "KKK") }
                };


    public static async Task<IReadOnlyCollection<Domain.Aggreagates.SignatoryAggregate.Signatory>> GetSignatories()
    {
        List<Domain.Aggreagates.SignatoryAggregate.Signatory> signatories = new()
        {
            new ("King", SignatoryRoleKey.K),
            new ("Notary", SignatoryRoleKey.N),
            new ("Validator", SignatoryRoleKey.V)
        };
        return await Task.FromResult(signatories);
    }

}
