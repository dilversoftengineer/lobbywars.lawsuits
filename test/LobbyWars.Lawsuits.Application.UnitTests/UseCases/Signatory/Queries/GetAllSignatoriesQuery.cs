﻿namespace LobbyWars.Lawsuits.Application.UseCases.Signatory.Queries;

public class GetAllSignatoriesQueryHandlerTests
{
    [Fact]
    public static async Task Handle_return_list_of_signatories()
    {
        var result = await Handle();
        Assert.True(result.Code == ResultCodeEnum.Ok);
    }

    public static async Task<Result<IReadOnlyCollection<GetAllSignatoriesQueryResult>>> Handle()
    {
        var signers =  await GetSignatories();
        var signatoryRepositoryReaderMock = new Mock<ISignatoryRepositoryReader>();
        signatoryRepositoryReaderMock.Setup(l=> l.All()).Returns(Task.FromResult(signers));

        var mapperMock = new Mock<IApplicationMapper>();
        mapperMock.Setup(l => l.Map<IReadOnlyCollection<GetAllSignatoriesQueryResult>>(new List<Domain.Aggreagates.SignatoryAggregate.Signatory>())).Returns(GetAllSignatoriesQueryResult());

        var getAllSignatoriesQueryHandler = new GetAllSignatoriesQueryHandler(Mock.Of<ILogger<GetAllSignatoriesQueryHandler>>(), signatoryRepositoryReaderMock.Object, mapperMock.Object);
        return await getAllSignatoriesQueryHandler.Handle(new GetAllSignatoriesQuery(), default);
    }

    public static async Task<IReadOnlyCollection<Domain.Aggreagates.SignatoryAggregate.Signatory>> GetSignatories()
    {
        List<Domain.Aggreagates.SignatoryAggregate.Signatory> signatories = new()
        {
            new ("King", SignatoryRoleKey.K),
            new ("Notary", SignatoryRoleKey.N),
            new ("Validator", SignatoryRoleKey.V)
        };
        return await Task.FromResult(signatories);
    }

    public static async Task<IReadOnlyCollection<GetAllSignatoriesQueryResult>> GetAllSignatoriesQueryResult()
    {
        List<GetAllSignatoriesQueryResult> signatories = new()
        {
            new (Guid.NewGuid(), SignatoryRoleKey.K.Name,"King", (short)SignatoryRoleKey.K.Id),
            new (Guid.NewGuid(), SignatoryRoleKey.K.Name,"Notary", (short)SignatoryRoleKey.N.Id),
            new (Guid.NewGuid(), SignatoryRoleKey.K.Name,"Validator", (short)SignatoryRoleKey.V.Id)
        };
        return await Task.FromResult(signatories);
    }
}
