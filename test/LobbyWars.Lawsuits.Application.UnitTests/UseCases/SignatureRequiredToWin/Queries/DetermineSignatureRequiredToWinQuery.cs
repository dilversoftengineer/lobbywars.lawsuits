﻿using LobbyWars.Lawsuits.Application.UseCases.Contract.Commands;

namespace LobbyWars.Lawsuits.Application.UseCases.SignatureRequiredToWin.Commands;

public class DetermineSignatureRequiredToWinQueryHandlerTests
{
    [Theory]
    [MemberData(nameof(Determine_lawsuit_party_winner_Cases))]
    public async Task Handle_with_valid_signatures(DetermineSignatureRequiredToWinQueryResult resultExpected, DetermineSignatureRequiredToWinQuery request)
    {
        var result = await Handle(request);
        Assert.Equal(resultExpected, result.Data);
    }
    [Theory]
    [MemberData(nameof(Handle_with_invalid_signatures_Cases))]
    public async Task Handle_with_invalid_signatures(DetermineSignatureRequiredToWinQuery request)
    {
        var result = await Handle(request);
        Assert.True(result.Code == ResultCodeEnum.BadRequest);
    }
    public async Task<Result<DetermineSignatureRequiredToWinQueryResult>> Handle(DetermineSignatureRequiredToWinQuery request)
    {
        var signers = await GetSignatories();
        var signatoryRepositoryReaderMock = new Mock<ISignatoryRepositoryReader>();
        signatoryRepositoryReaderMock.Setup(l => l.All()).Returns(Task.FromResult(signers));

        var DetermineSignatureRequiredToWinQueryQueryHandler = new DetermineSignatureRequiredToWinQueryHandler(Mock.Of<ILogger<DetermineSignatureRequiredToWinQueryHandler>>(), new SignatoryService(Mock.Of<ILogger<SignatoryService>>()), new ContractService(Mock.Of<ILogger<ContractService>>(), new SignatoryService(Mock.Of<ILogger<SignatoryService>>())), signatoryRepositoryReaderMock.Object);
        return await DetermineSignatureRequiredToWinQueryQueryHandler.Handle(request, default);
    }


    public static IEnumerable<object[]> Determine_lawsuit_party_winner_Cases => new List<object[]>
                {
                    new object[] { new DetermineSignatureRequiredToWinQueryResult("N", ""), new DetermineSignatureRequiredToWinQuery("N#V", "NVV") },
                    new object[] { new DetermineSignatureRequiredToWinQueryResult("K", ""), new DetermineSignatureRequiredToWinQuery("KV", "N#K") },
                    new object[] { new DetermineSignatureRequiredToWinQueryResult("", "K"), new DetermineSignatureRequiredToWinQuery("K#N", "V#K") },
                    new object[] { new DetermineSignatureRequiredToWinQueryResult("N", "N"), new DetermineSignatureRequiredToWinQuery("K#N", "K#N") }
                };

    public static IEnumerable<object[]> Handle_with_invalid_signatures_Cases => new List<object[]>
                {
                    new object[] { new DetermineSignatureRequiredToWinQuery("N##", "NVV") },
                    new object[] { new DetermineSignatureRequiredToWinQuery("XXX", "N#K") },
                    new object[] { new DetermineSignatureRequiredToWinQuery("KKN", "V##") },
                    new object[] { new DetermineSignatureRequiredToWinQuery("K#E", "K#1") }
                };

    public static async Task<IReadOnlyCollection<Domain.Aggreagates.SignatoryAggregate.Signatory>> GetSignatories()
    {
        List<Domain.Aggreagates.SignatoryAggregate.Signatory> signatories = new()
        {
            new ("King", SignatoryRoleKey.K),
            new ("Notary", SignatoryRoleKey.N),
            new ("Validator", SignatoryRoleKey.V)
        };
        return await Task.FromResult(signatories);
    }
    public static async Task<IReadOnlyCollection<GetAllSignatoriesQueryResult>> GetAllSignatoriesQueryResult()
    {
        List<GetAllSignatoriesQueryResult> signatories = new()
        {
            new (Guid.NewGuid(), SignatoryRoleKey.K.Name,"King", (short)SignatoryRoleKey.K.Id),
            new (Guid.NewGuid(), SignatoryRoleKey.K.Name,"Notary", (short)SignatoryRoleKey.N.Id),
            new (Guid.NewGuid(), SignatoryRoleKey.K.Name,"Validator", (short)SignatoryRoleKey.V.Id)
        };
        return await Task.FromResult(signatories);
    }
}
